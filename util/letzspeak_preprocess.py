import os
import pickle

from tqdm import tqdm
from config import *
import pandas as pd
# from utils import ensure_folder


# def get_data(mode):
#     print('getting {} data...'.format(mode))

#     global VOCAB

#     with open(tran_file, 'r', encoding='utf-8') as file:
#         lines = file.readlines()

#     tran_dict = dict()
#     for line in lines:
#         tokens = line.split()
#         key = tokens[0]
#         trn = ''.join(tokens[1:])
#         tran_dict[key] = trn

#     samples = []

#     folder = os.path.join(wav_folder, mode)
#     ensure_folder(folder)
#     dirs = [os.path.join(folder, d) for d in os.listdir(folder) if os.path.isdir(os.path.join(folder, d))]
#     for dir in tqdm(dirs):
#         files = [f for f in os.listdir(dir) if f.endswith('.wav')]

#         for f in files:
#             wave = os.path.join(dir, f)

#             key = f.split('.')[0]
#             if key in tran_dict:
#                 trn = tran_dict[key]
#                 trn = list(trn.strip()) + ['<EOS>']

#                 for token in trn:
#                     build_vocab(token)

#                 trn = [VOCAB[token] for token in trn]

#                 samples.append({'trn': trn, 'wave': wave})

#     return samples

def process_rows(sliced_df):
    global VOCAB
    samples = []
    for row in tqdm(range(sliced_df.shape[0])):
        wave = os.path.join(CLIPS_FOLDER, sliced_df.iloc[row]['filename'])
        sentence = sliced_df.iloc[row]['text']
        sentence_length = len(sentence.strip().split())
        # if sentence_length <= 10 or sentence_length > 30:
        #     continue
        trn = list(sentence.strip()) + ['<EOS>']
        build_vocab(trn)
        trn = [VOCAB[token] for token in trn]
        samples.append({'trn': trn, 'wave': wave})
    return samples


def get_data():
    global VOCAB
    df = pd.read_csv(TRANSCRIPT_FILE)
    df = df.sample(frac=1)
    total_rows = df.shape[0]
    train_rows = round(total_rows * 0.8)
    dev_rows = round((total_rows - train_rows) * 0.75)
    df_train = df[:train_rows]
    df_dev = df[train_rows:train_rows + dev_rows]
    df_test = df[train_rows + dev_rows:]

    return process_rows(df_train), process_rows(df_dev), process_rows(df_test)


def build_vocab(trn):
    global VOCAB, IVOCAB
    for token in trn:
        if not token in VOCAB:
            next_index = len(VOCAB)
            VOCAB[token] = next_index
            IVOCAB[next_index] = token


if __name__ == "__main__":
    VOCAB = {'<PAD>': 0, '<SOS>': 1, '<EOS>': 2, ' ': 3}
    IVOCAB = {0: '<PAD>', 1: '<SOS>', 2: '<EOS>', 3: ' '}

    data = dict()
    data['VOCAB'] = VOCAB
    data['IVOCAB'] = IVOCAB
    get_data()
    data['train'], data['dev'], data['test'] = get_data()

    with open(PICKLE_FILE, 'wb') as file:
        pickle.dump(data, file)

    print('num_train: ' + str(len(data['train'])))
    print('num_dev: ' + str(len(data['dev'])))
    print('num_test: ' + str(len(data['test'])))
    print('vocab_size: ' + str(len(data['VOCAB'])))
