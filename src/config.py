import os
import torch


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')  # sets device for model and PyTorch tensors

# Model parameters
input_dim = 40  # dimension of feature
window_size = 25  # window size for FFT (ms)
hidden_size = 512
embedding_dim = 512
stride = 10  # window stride for FFT
cmvn = True  # apply CMVN on feature
num_layers = 4

# Training parameters
# batch_size = 32
batch_size = 1
lr = 1e-3
num_workers = 8  # for data-loading
grad_clip = 5.  # clip gradients at an absolute value of
print_freq = 10  # print training/validation stats  every __ batches
checkpoint = None  # path to checkpoint, None if none

# Data parameters
PAD_token = 0
SOS_token = 1
EOS_token = 2
num_train = 120418 #0.87
num_dev = 14326 #0.13
num_test = 7176 #0.05
vocab_size = 4336

# DATA_DIR = 'data'
# aishell_folder = 'data/data_aishell'
# wav_folder = os.path.join(aishell_folder, 'wav')
# tran_file = os.path.join(aishell_folder, 'transcript/aishell_transcript_v0.8.txt')
# IMG_DIR = 'data/images'
# pickle_file = 'data/aishell.pickle'

# LETZSPEAK_DIR = 'letzspeak-dataset'
# CLIPS_FOLDER = os.path.join(LETZSPEAK_DIR, 'clips')
# TRANSCRIPT_FILE = os.path.join(LETZSPEAK_DIR, 'letzspeak_dataset.csv')
# IMG_DIR = os.path.join(LETZSPEAK_DIR, 'images')
# PICKLE_FILE = os.path.join(LETZSPEAK_DIR, 'letzspeak_dataset.pickle')

LIBRISPEECH_DIR = 'corpus/librispeech'
LIBRISPEECH_TRAIN = 'corpus/librispeech/train.csv'
# LIBRISPEECH_SMALL_TRAIN = 'corpus/librispeech/one_example_train.csv'
LIBRISPEECH_SMALL_TRAIN = 'corpus/librispeech/small_train.csv'
LIBRISPEECH_DEV = 'corpus/librispeech/dev.csv'
# LIBRISPEECH_SMALL_DEV = 'corpus/librispeech/one_example_dev.csv'
LIBRISPEECH_SMALL_DEV = 'corpus/librispeech/small_dev.csv'
LIBRISPEECH_TEST = 'corpus/librispeech/test.csv'

CHECKPOINT_FOLDER = 'checkpoint/'
