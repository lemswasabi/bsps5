import torch
import pickle
import numpy as np
import pandas as pd

# from utils import extract_feature
from torch.utils.data import Dataset
from .config import num_workers
from torch.utils.data.dataloader import default_collate


def pad_collate(batch):
    max_input_len = float('-inf')
    max_target_len = float('-inf')

    for elem in batch:
        feature, trn = elem
        max_input_len = max_input_len if max_input_len > feature.shape[0] else feature.shape[0]
        max_target_len = max_target_len if max_target_len > len(trn) else len(trn)

    for i, elem in enumerate(batch):
        f, trn = elem
        input_length = f.shape[0]
        input_dim = f.shape[1]
        # print('f.shape: ' + str(f.shape))
        feature = np.zeros((max_input_len, input_dim), dtype=np.float)
        feature[:f.shape[0], :f.shape[1]] = f
        trn = np.pad(trn, (0, max_target_len - len(trn)), 'constant', constant_values=0)
        batch[i] = (feature, trn, input_length)
        # print('feature.shape: ' + str(feature.shape))
        # print('trn.shape: ' + str(trn.shape))

    batch.sort(key=lambda x: x[2], reverse=True)

    return default_collate(batch)


class LibriSpeechDataset(Dataset):
    def __init__(self, dataset_filepath):
        self.samples_df = pd.read_csv(dataset_filepath)

    def __getitem__(self, i):
        sample = self.samples_df.iloc[i]
        feature_filename = sample['input']
        feature = np.load(feature_filename)
        label = list(map(int, sample['label'].split()))
        return feature, label

    def __len__(self):
        return self.samples_df.shape[0]


if __name__ == "__main__":
    train_dataset = LibriSpeechDataset('corpus/librispeech/train.csv')
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=256,
                                               shuffle=True,
                                               collate_fn=pad_collate,
                                               num_workers=num_workers,
                                               pin_memory=True)

    print(train_dataset.__getitem__(0))
    print(len(train_dataset))
    print(len(train_loader))
    # print(list(train_loader)[0])
